# Cable Quality Tracker Automation Script

This Python script automates the process of generating a Markdown report for cable testing and updating a CSV file that tracks the quality of cables.

## Requirements

- Python 3.x

## How to Use

1. Place the script in the same directory as the `CableQualityTracker.csv` and the serial number-named folder.
2. The serial number-named folder should contain the required files (Any file name is acceptable, script searches for the file extension):
    - A png from the power delivery test
    - A csv from the power delivery test
    - A txt from the data transmission test

NOTE: You can update the pass/fail criteria for power delivery and data transmission within the script. Default is set to: 
```python
    # Set test threshold values
    avgDataRate_thresholdMbs = 885  # Mb/s
    voltageAt700mA_threshold = 4.7  # V
```
3. Run the script by executing `python CableTestReportGenerator.py` in the command line or terminal.
4. Enter the serial number of the cable when prompted, this should also be the folder name with the required files.
5. The script will rename the files in the serial number-named folder to include the serial number.
6. The script will extract data from the Data Transmission TXT file.
7. Enter the required information when prompted by the script.
8. The script will generate a Markdown report with the entered information and the extracted data.
9. The script will update the `CableQualityTracker.csv` file with the relevant information.

## Note

- The user will need to manually insert the results and GitLab issue link later in the Cable Quality Tracker.
- Ensure that the files and directories are in the expected locations and that you have the necessary permissions to write files.

## Example Directory Structure

```
project_directory/
│
├── CableQualityTracker.csv
├── CableQualityTracker.py
│
└── SERIAL_NUMBER/
├── Power Delivery.png
├── Power Delivery.csv
└── Data Transmission.txt
```