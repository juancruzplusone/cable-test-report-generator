import os
import re
import csv
from datetime import datetime


def file_rename(serial_number):
    """
    This function will rename the files in the directory to match the naming convention
    """
    file_types = ['.png', '.csv', '.txt']
    for file_type in file_types:
        file_found = False
        for file_in_dir in os.listdir(serial_number):
            if file_in_dir.lower().endswith(file_type):
                old_path = os.path.join(serial_number, file_in_dir)
                if file_type == '.png':
                    new_name = f"{serial_number}_PowerDelivery{file_type}"
                elif file_type == '.csv':
                    new_name = f"{serial_number}_PowerDelivery{file_type}"
                elif file_type == '.txt':
                    new_name = f"{serial_number}_DataTransmission{file_type}"
                new_path = os.path.join(serial_number, new_name)
                os.rename(old_path, new_path)
                file_found = True
                break

        if not file_found:
            print(
                f"File of type {file_type} not found in directory {serial_number}")
            
            raise FileNotFoundError(f"File of type {file_type} not found in directory {serial_number}")


    print("Files renaming complete...")


def get_data_transmission_variables(serial_number, avgDataRate_thresholdMbs):
    """
    This function will parse the DataTransmission file and return a dictionary of variables to be used in the report. Determines pass/fail based on avg_rate
    """
    data_transmission_file = os.path.join(
        serial_number, f"{serial_number}_DataTransmission.txt")

    # Initialize an empty dictionary
    report_variables = {}

    # Initialize variables with default value of "NULL"
    # We do this since DT test sometimes does not complete for bad cables
    report_variables["data_transmission_test_duration"] = "NULL"
    report_variables["max_rate"] = ["NULL", "NULL"]
    report_variables["min_rate"] = ["NULL", "NULL"]
    report_variables["avg_rate"] = ["NULL", "NULL"]
    report_variables["error_count"] = "NULL"

    try:
        with open(data_transmission_file, 'r') as file:
            lines = file.readlines()
            results_section = False
            for line in lines:
                if "RESULTS:" in line:
                    results_section = True
                if results_section:
                    if "Duration:" in line:
                        report_variables["data_transmission_test_duration"] = re.search(
                            r"(\d+)s", line).group(1)
                    if "Maximum Data Rate" in line:
                        report_variables["max_rate"] = re.findall(
                            r"(\d+\.\d+)", line)
                    if "Minimum Data Rate" in line:
                        report_variables["min_rate"] = re.findall(
                            r"(\d+\.\d+)", line)
                    if "Average Rate" in line:
                        report_variables["avg_rate"] = re.findall(
                            r"(\d+\.\d+)", line)
                    if "error(s)" in line:
                        report_variables["error_count"] = re.search(
                            r"(\d+) error\(s\)", line).group(1)
                        break

        # Convert avg_rate to float and use as threshold to determine pass/fail
        # report_variables['max_rate'][0] is the rate in Mb/s. Should not be NULL and should be greater than 1200 Mb/s, then consider Pass
        if report_variables["avg_rate"][0] != "NULL" and float(report_variables["avg_rate"][0]) >= avgDataRate_thresholdMbs:
            report_variables["data_transmission_test_results"] = "Pass"
            report_variables["data_transmission_test_notes"] = f"The average data rate was {report_variables['avg_rate'][0]} Mb/s, which is above the threshold of {avgDataRate_thresholdMbs} Mb/s."
        else:
            report_variables["data_transmission_test_results"] = "Fail"
            if report_variables["avg_rate"][0] != "NULL":
                report_variables["data_transmission_test_notes"] = f"The average data rate was {report_variables['avg_rate'][0]} Mb/s, which is below the threshold of {avgDataRate_thresholdMbs} Mb/s."
            else:
                report_variables["data_transmission_test_notes"] = "The average data rate could not be determined."

        print(
            f"Data Transmission results message: {report_variables['data_transmission_test_notes']}")

    except Exception as e:
        print(f"An error occurred while processing file: {e}")
        raise e

    return report_variables


def get_power_delivery_variables(serial_number, voltageAt700mA_threshold):
    """
    Opens the PowerDelivery CSV file, finds voltage that is in same row as 700mA current,
    creates a report_variable[voltage_at_700mA], uses it to determine compliance based on threshold input, and then returns report_variables
    """
    power_delivery_file = os.path.join(
        serial_number, f"{serial_number}_PowerDelivery.csv")

    # Initialize an empty dictionary
    report_variables = {}

    try:
        with open(power_delivery_file, 'r') as file:
            reader = csv.DictReader(file, delimiter=',')
            for row in reader:
                if row[' Current (mA)'] == '700':
                    report_variables["voltage_at_700mA"] = row['Voltage (V)']
                    break

        # Determine compliance based on threshold input
        if report_variables["voltage_at_700mA"] != "NULL":
            if float(report_variables["voltage_at_700mA"]) >= voltageAt700mA_threshold:
                report_variables["power_delivery_test_results"] = "Pass"
                report_variables["power_delivery_test_notes"] = f"The voltage at 700mA was {report_variables['voltage_at_700mA']}V, which is above the threshold of {voltageAt700mA_threshold}V."
            else:
                report_variables["power_delivery_test_results"] = "Fail"
                report_variables["power_delivery_test_notes"] = f"The voltage at 700mA was {report_variables['voltage_at_700mA']}V, which is below the threshold of {voltageAt700mA_threshold}V."

        print(f"Power Delivery results message: {report_variables['power_delivery_test_notes']}")

    except Exception as e:
        print(f"An error occurred while processing file: {e}")
        raise e

    return report_variables


def get_user_input_variables(serial_number):
    """
    This function will parse the PowerDelivery and DataTransmission files and return a dictionary of variables to be used in the report

    """
    # Initialize an empty dictionary
    report_variables = {}

    report_variables["cable_type"] = input(
        "Enter USB Cable Type (EX: USB Male A to Male C): ")
    report_variables["cable_length"] = input("Enter Cable Length: ")
    report_variables["manufacturer"] = input("Enter Manufacturer: ")
    report_variables["physical_condition"] = input(
        "Enter Physical Condition (Visual Observation): ")

    report_variables["conclusion_pass_fail"] = input(
        "Enter Conclusion (Pass/Fail): ")
    report_variables["results_statement"] = input("Enter Results Statement: ")
    report_variables["gitlab_username"] = input(
        "Enter your GitLab username(Ommit the @): ")

    return report_variables


def generate_report(report_variables):
    """
    This function will generate the report in Markdown format
    """
    markdown_template = f"""# (SN: {report_variables['serial_number']}) Cable Test Results

## Cable Specifications

**Cable Type:** {report_variables['cable_type']}

**Cable Length:** {report_variables['cable_length']}

**Manufacturer:** {report_variables['manufacturer']}

**Physical Condition (Visual Observation):** {report_variables['physical_condition']}


## Power Delivery Test

**Voltage vs Current Compliance:** {report_variables['power_delivery_test_results']}

(insert results here when creating gitlab issue : Pic and CSV)

**Voltage at 700mA:** {report_variables['voltage_at_700mA']}

**Result Notes:** {report_variables['power_delivery_test_notes']}


## Data Transmission Test

**Data Transfer Compliance:** {report_variables['data_transmission_test_results']}

**Loopback Test Duration:** {report_variables['data_transmission_test_duration']} seconds

| Metric                 | Rate (Mb/s) | Rate (MB/s) |
|------------------------|-------------|-------------|
| Maximum Read Data Rate | {report_variables['max_rate'][0]} | {report_variables['max_rate'][1]} |
| Minimum Read Data Rate | {report_variables['min_rate'][0]} | {report_variables['min_rate'][1]} |
| Average Read Data Rate | {report_variables['avg_rate'][0]} | {report_variables['avg_rate'][1]} |

*Note: The data transfer rates are provided in both Megabits per second (Mb/s) and Megabytes per second (MB/s).*

**Error Count:** {report_variables['error_count']}

(insert result files here when creating gitlab issue: TXT)

**Result Notes:** {report_variables['data_transmission_test_notes']}


## Conclusion

**Pass/Fail:** {report_variables['conclusion_pass_fail']}

**Results Statement:** {report_variables['results_statement']}

**Conducted By:** @{report_variables['gitlab_username']}

**Date Testing Conducted:** {datetime.now().strftime("%m-%d-%Y")}
    """

    try:
        with open(os.path.join(report_variables['serial_number'], f"{report_variables['serial_number']}_report.md"), 'w') as file:
            file.write(markdown_template)

        print(f"Markdown report generated successfully in {report_variables['serial_number']} folder")

    except Exception as e:
        print(f"An error occurred while writing the Markdown report: {e}")
        raise e


def update_cable_quality_tracker(report_variables):
    """
    This function will update the CableQualityTracker.csv file with the new test results
    """
    csv_file = 'CableQualityTracker.csv'

    headers = ["Date Tested", "Manufacturer", "Serial Number", "Cable Type", "Cable Length",
               "Maximum Data Rate (Mb/s)", "Minimum Data Rate (Mb/s)", "Data Transmission Results (Avg Read Rate)", "Data Transmission Compliance (Pass/Fail)",
               "Voltage at 700mA", "Voltage vs Current Compliance (Pass/Fail)",
               "Cable Pass/Fail", "Gitlab Issue Link"]

    try:
        # Check if file exists, if not, write headers
        if not os.path.isfile(csv_file):
            with open(csv_file, mode='w', newline='') as file:
                writer = csv.writer(file)
                writer.writerow(headers)
        else:
            # Read the existing data
            with open(csv_file, mode='r', newline='') as file:
                reader = csv.reader(file)
                data = list(reader)

            # Update the data if the serial number exists, otherwise append new data
            # TODO: This may not be good if writing to actual quality tracker since we would like to track same serial number multiple times
            for i, row in enumerate(data):
                if row[2] == report_variables['serial_number']:
                    data[i] = [datetime.now().strftime("%m-%d-%Y"), report_variables['manufacturer'], report_variables['serial_number'], report_variables['cable_type'], report_variables['cable_length'],
                               report_variables['max_rate'][0], report_variables['min_rate'][0], report_variables['avg_rate'][0],
                               report_variables['data_transmission_test_results'], report_variables[
                                   'voltage_at_700mA'], report_variables['power_delivery_test_results'],
                               report_variables['conclusion_pass_fail'], "Gitlab Issue Link"]
                    break
            else:
                data.append([datetime.now().strftime("%m-%d-%Y"), report_variables['manufacturer'], report_variables['serial_number'], report_variables['cable_type'], report_variables['cable_length'],
                             report_variables['max_rate'][0], report_variables['min_rate'][0], report_variables['avg_rate'][0],
                             report_variables['data_transmission_test_results'], report_variables[
                                 'voltage_at_700mA'], report_variables['power_delivery_test_results'],
                             report_variables['conclusion_pass_fail'], "Gitlab Issue Link"])

            # Write the data back to the CSV file
            with open(csv_file, mode='w', newline='') as file:
                writer = csv.writer(file)
                writer.writerows(data)

        print(f"CableQualityTracker.csv updated successfully")

    except Exception as e:
        print(f"An error occurred while updating CableQualityTracker.csv: {e}")
        raise e



if __name__ == "__main__":
    report_variables = {}  # Dictionary to store all variables needed for report

    # Set test threshold values
    avgDataRate_thresholdMbs = 885  # Mb/s
    voltageAt700mA_threshold = 4.7  # V

    report_variables['serial_number'] = input(
        "Enter the serial number (Should be name of folder to test): ")

    file_rename(report_variables['serial_number'])

    report_variables.update(get_data_transmission_variables(
        report_variables['serial_number'], avgDataRate_thresholdMbs))

    report_variables.update(get_power_delivery_variables(
        report_variables['serial_number'], voltageAt700mA_threshold))

    
    report_variables.update(get_user_input_variables(report_variables['serial_number']))

    generate_report(report_variables)
    update_cable_quality_tracker(report_variables)

    print("Program done.")
